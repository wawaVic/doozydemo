﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum GetHolidaySourceType {
    index
    , month
    , specDate
}

public class HolidayInfo : MonoBehaviour {

    string sourceURL = "https://quality.data.gov.tw/dq_download_json.php?nid=14718&md5_url=78eba9e4421f1c9d33149f060533691c";
    static public HolidayDate[] resourceRawData;
    static public HolidayInfo Instance;

    public HolidayInfo() {
        Instance = this;
    }

    private void Awake() {
        //InitSourceData();
    }

    private void Start() {
        InitSourceData();
    }

    public void InitSourceData() {
        StartCoroutine(InitRawData());
    }

    /// <summary>
    /// 把URL資料裝到陣列裡
    /// </summary>
    /// <returns></returns>
    IEnumerator InitRawData() {
        UnityWebRequest webRequest = UnityWebRequest.Get(sourceURL);

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError) {
            Debug.LogError(webRequest.error);
            yield break;
        }

        string resourceJsonString = webRequest.downloadHandler.text;
        string resourceTextModi = "{\"Items\":{data}}".Replace("{data}", resourceJsonString);
        resourceRawData = JSONParse.FromJson<HolidayDate>(resourceTextModi);

        //Json陣列轉換test
        /*
        HolidayDate[] holiDateArray = new HolidayDate[2];
        for (int i = 0; i < holiDateArray.Length; i++) {
            holiDateArray[i] = new HolidayDate();
            holiDateArray[i].date = "20200711 12";
            holiDateArray[i].week = i.ToString(); ;
            holiDateArray[i].holiday = (i + 1).ToString(); ;
            holiDateArray[i].remark = "i= " + i.ToString();
        }

        string JSONtestString = JSONParse.ToJson(holiDateArray, false);
        Debug.Log(JSONtestString);
        JSONTest[] holidayTest = JSONParse.FromJson<JSONTest>(JSONtestString);
        Debug.Log("==========");

        Debug.Log(holidayTest[0].date);
        Debug.Log(holidayTest[0].week);
        Debug.Log(holidayTest[0].holiday);
        Debug.Log(holidayTest[0].remark);
        Debug.Log("==========");
        Debug.Log(holidayTest[1].date);
        Debug.Log(holidayTest[1].week);
        Debug.Log(holidayTest[1].holiday);
        Debug.Log(holidayTest[1].remark);
        */
        //Json陣列轉換test
    }

    public HolidayDate[] Get<T>(GetHolidaySourceType conditionType, T condition) {
        HolidayDate[] result = null;
        switch (conditionType) {
            case GetHolidaySourceType.index:
                result = GetByIndex(int.Parse(condition.ToString()));
                break;
            case GetHolidaySourceType.month:
                result = GetByMonth(int.Parse(condition.ToString()));
                break;
            case GetHolidaySourceType.specDate:
                result = GetBySpecDate(condition.ToString());
                break;
            default:
                break;
        }

        return result;
    }

    private HolidayDate[] GetByIndex(int index) {
        HolidayDate[] result = new HolidayDate[1];
        //檢查資料區間
        if (index >= 0 && index < resourceRawData.Length) {
            result[0] = resourceRawData[index];
        } else {
            Debug.Log("HolidayInfo: 資料超出範圍");
        }
        return result;
    }

    private HolidayDate[] GetByMonth(int index) {
        HolidayDate[] result = new HolidayDate[31];
        int resultIndex = 0;
        //檢查資料區間
        if (index >= 1 && index <= 12) {
            foreach (HolidayDate date in resourceRawData) {
                if (date.GetDate().Month == index) {
                    result[resultIndex] = date;
                    resultIndex++;
                }
            }
        } else {
            Debug.Log("HolidayInfo: 月份條件超出範圍");
        }
        return result;
    }

    public HolidayDate[] GetBySpecDate(string specDate) {
        HolidayDate[] result = new HolidayDate[1];
        foreach (HolidayDate date in resourceRawData) {
            string ori = date.GetDateOri();
            if (date.GetDateOri() == specDate) {
                result[0] = date;
            }
        }
        return result;
    }
}

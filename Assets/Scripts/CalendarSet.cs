﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CalendarSet : MonoBehaviour {

    public RectTransform content;

    GameObject backgroundPre;
    GameObject monthTextPre;
    GameObject dateTextPre;
    RectTransform monthSeparate;    //background底下的Month頭
    RectTransform dateBody;         //background底下的dateBody

    HolidayDate[] monthData;

    string thisYear;

    float monthHeadHeight;
    float monthHeadWidth;
    float dateBodyHeight;
    float dateTextHeight;
    float dateTextWidth;
    float sideKeep;

    // Start is called before the first frame update
    void Start() {
        backgroundPre = Resources.Load("Calendar/CalendarBackground") as GameObject;
        monthTextPre = Resources.Load("Calendar/Month") as GameObject;
        dateTextPre = Resources.Load("Calendar/Date") as GameObject;

        monthHeadHeight = ValueManager.instance.CalendarFieldVal["monthHeadHeight"];
        monthHeadWidth = ValueManager.instance.CalendarFieldVal["monthHeadWidth"];
        dateBodyHeight = ValueManager.instance.CalendarFieldVal["dateBodyHeight"];
        dateTextHeight = ValueManager.instance.CalendarFieldVal["dateTextHeight"];
        dateTextWidth = ValueManager.instance.CalendarFieldVal["dateTextWidth"];
        sideKeep = ValueManager.instance.CalendarFieldVal["sideKeep"];

        thisYear = "2020";

        GenerateCalendar();
        //CalendarDataTest(); //debug
    }

    private void GenerateCalendar() {
        float monthNum = 12;
        float currentWidth = content.rect.width;
        string thisYear = "2020";
        float dateBodySum = 0;

        //一次產生一個月的Background + data
        for (int i = 1; i <= monthNum; i++) {
            //取得當月週數以決定Background高度
            monthData = HolidayInfo.Instance.Get(GetHolidaySourceType.month, i);
            int weekMax = 0;
            //從最後面往前找，判定當月最後一天
            for (int j = 30; j >= 0; j--) {
                if (monthData[j] == null) {
                    continue;
                }
                weekMax = GetWeekOfMonth(monthData[0].GetWeek(), monthData[j].GetDate().Day);
                break;
            }
            //Debug.Log("Month: " + i + " MaxWeek: " + weekMax);

            GameObject calendarBG;
            //依照已經過月份數的monthHeadHeight數量 + 上每次疊加的dateBodyHeight，決定當月要放的位置
            float newBackgroundPosY = (monthHeadHeight * (i - 1) + dateBodySum);
            calendarBG = Instantiate(backgroundPre, new Vector2(0.0f, 0.0f), Quaternion.identity);
            calendarBG.transform.SetParent(content);
            calendarBG.GetComponent<RectTransform>().anchoredPosition = new Vector2(0.0f, -newBackgroundPosY);
            monthSeparate = calendarBG.transform.Find("MonthSeparate").GetComponent<RectTransform>();
            dateBody = calendarBG.transform.Find("DateBody").GetComponent<RectTransform>();
            //定義background DateBody長度
            float currentDateBodyWidth = dateBody.rect.width;
            float currDateBodyHeight = dateBodyHeight * weekMax;

            dateBody.sizeDelta = new Vector2(currentDateBodyWidth, currDateBodyHeight);
            dateBodySum += currDateBodyHeight;
            calendarBG.name = thisYear + i.ToString("00");

            //在行事曆background中加入資料
            InsertData(i);
        }

        //算出一年(12個月所需的行事曆長度)
        float newContentViewHeight = (monthHeadHeight * monthNum) + dateBodySum;
        content.sizeDelta = new Vector2(currentWidth, newContentViewHeight);
    }

    private void InsertData(int month) {
        //string yearCharacter = "年";
        string monthCharacter = "月";
        //string dayCharacter = "日";

        Color holidayColor = Color.red;

        //把月份名稱(1月)放到該月第一天的星期幾對應位置
        int firstDateOfMonthWeek = monthData[0].GetWeek();
        //由於星期日在視覺上是在左邊看來第一格
        if (firstDateOfMonthWeek == 7) {
            firstDateOfMonthWeek = 0;
        }
        GameObject monthText;
        float newMonthTextPosX = (monthHeadWidth * firstDateOfMonthWeek) + sideKeep;
        monthText = Instantiate(monthTextPre, new Vector2(0.0f, 0.0f), Quaternion.identity);
        monthText.transform.SetParent(monthSeparate);
        monthText.GetComponent<RectTransform>().anchoredPosition = new Vector2(newMonthTextPosX, 0.0f);
        TextMeshProUGUI monthTextContent = monthText.GetComponent<TextMeshProUGUI>();
        monthTextContent.text = month + monthCharacter;
        monthText.name = thisYear + month.ToString("00") + "Text";

        //每日放到對應位置，填上節日資訊、西元日期
        foreach (HolidayDate date in monthData) {
            if (date == null) {
                break;
            }
            int weekNum = GetWeekOfMonth(monthData[0].GetWeek(), date.GetDate().Day);
            weekNum--;
            GameObject dateTextPreImp;
            int dateOfWeek = date.GetWeek();
            //由於星期日在視覺上是在左邊看來第一格
            if (dateOfWeek == 7) dateOfWeek = 0;
            float newDateTextPosX = (dateTextWidth * dateOfWeek) + sideKeep;
            float newDateTextPosY = (dateTextHeight * weekNum);
            dateTextPreImp = Instantiate(dateTextPre, new Vector2(0.0f, 0.0f), Quaternion.identity);
            dateTextPreImp.transform.SetParent(dateBody);
            dateTextPreImp.GetComponent<RectTransform>().anchoredPosition = new Vector2(newDateTextPosX, -newDateTextPosY);
            //TextMeshProUGUI dateTextContent = monthText.GetComponent<TextMeshProUGUI>();

            TextMeshProUGUI dateTextContent = dateTextPreImp.transform.Find("DateText").GetComponent<TextMeshProUGUI>();
            Text remark = dateTextPreImp.transform.Find("Remark").GetComponent<Text>();

            dateTextContent.text = date.GetDate().Day.ToString();
            remark.text = date.GetRemark();
            //放假日字改成紅色
            if (date.GetHoliday() == "2") {
                dateTextContent.color = holidayColor;
                remark.color = holidayColor;
            }
            //依照假日資訊 調整字體大小
            int remakeTextNum = date.GetRemark().Length;
            int defaultRemarkSize = 25;
            float remarkWidth = dateTextWidth - (sideKeep * 2);
            float remarkTextSize = remarkWidth / remakeTextNum;
            if (remarkTextSize >= defaultRemarkSize) remarkTextSize = defaultRemarkSize;
            remark.fontSize = (int)remarkTextSize;

            dateTextPreImp.name = date.GetDateOri();
            //Debug.Log("DAY: " + date.GetDate().Day + " Week: " + week);
        }
    }

    private int GetWeekOfMonth(int firstWeek, int date) {
        int result = 0;
        int weekOne, weekTwo, weekThree, weekFour, weekFive, weekSix;
        //差，給第一周計算`
        int daysOfWeek = 7;
        //禮拜日在最左
        if (firstWeek == 7) firstWeek = 0;
        int diff = daysOfWeek - firstWeek;
        weekOne = 1 + diff;
        weekTwo = weekOne + daysOfWeek;
        weekThree = weekTwo + daysOfWeek;
        weekFour = weekThree + daysOfWeek;
        weekFive = weekFour + daysOfWeek;
        weekSix = weekFive + daysOfWeek;
        if (1 <= date && date < weekOne) {
            result = 1;
        } else if (weekOne <= date && date < weekTwo) {
            result = 2;
        } else if (weekTwo <= date && date < weekThree) {
            result = 3;
        } else if (weekThree <= date && date < weekFour) {
            result = 4;
        } else if (weekFour <= date && date < weekFive) {
            result = 5;
        } else if (weekFive <= date && date < weekSix) {
            result = 6;
        }
        return result;
    }

    //test
    public void CalendarDataTest() {
        HolidayDate[] currDateData;
        //index
        /* 
        int numMax = 366;
        for (int i = 0; i < numMax; i++) {
            currDateData = HolidayInfo.Instance.Get(GetHolidaySourceType.index, i);
            //currDateData = HolidayInfo.Instance.Get(i);
            if (currDateData == null) continue;
            Debug.Log(i.ToString() + ": " + currDateData[0].GetDate().ToString());
        }
        */
        //specDate
        string specDate = "20200523";
        currDateData = HolidayInfo.Instance.Get(GetHolidaySourceType.specDate, specDate);
        if (currDateData != null) {
            Debug.Log(currDateData[0].GetDateOri() + " Done!!!");
        } else {
            Debug.Log("CalendarDataTest not find");
        }
    }
}

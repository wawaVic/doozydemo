﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinusDataField : MonoBehaviour {

    public GameObject headObject;
    ScrollDataFieldController scrollController;

    // Start is called before the first frame update
    void Start() {
        scrollController = GameObject.Find("Content_DataField").GetComponent<ScrollDataFieldController>();
    }

    public void minusThisDataField() {
        scrollController.MinusDataField(headObject);
        //scrollController.minusDataField(transform.parent.gameObject.name.ToString());
        //Destroy(transform.parent.gameObject);
    }
}

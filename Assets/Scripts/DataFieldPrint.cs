﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DataFieldPrint : MonoBehaviour {

    public TextMeshProUGUI nameContent;
    public TextMeshProUGUI ageContent;
    public Toggle checkContent;

    TextMeshProUGUI showArea;

    
    // Start is called before the first frame update
    void Start() {
        showArea = GameObject.Find("ContentShowArea").GetComponent<TextMeshProUGUI>();
    }

    public void printThisContent() {
        string checkResult;
        string nextRow = "\n";
        if (checkContent.isOn) {
            checkResult = "Check";
        } else {
            checkResult = "UnCheck";
        }
        string showContent = "Name: " + nameContent.text + nextRow +
            "Age: " + ageContent.text + nextRow +
            "Check: " + checkResult;
        showArea.text = showContent;
    }
}

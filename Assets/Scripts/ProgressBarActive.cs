﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBarActive : MonoBehaviour {

    GameObject progressBarGroup;

    // Start is called before the first frame update
    void Start() {
        progressBarGroup = this.gameObject;
        progressBarGroup.SetActive(false);
    }

    public void setActive(bool active) {
        if (active) {
            progressBarGroup.SetActive(true);
        } else {
            progressBarGroup.SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollDataFieldController : MonoBehaviour {

    GameObject dataFieldPrefab;

    float currentIndex;
    float currentNameIndex;
    float fieldHeight;
    float contentHeight;
    float contentHeightMin;
    float plusButtonHeight;
    float spacingTop;
    float spacingBottom;
    float spacingBetween;
    char separate;
    string dataFieldName;
    Transform contentTrans;
    Vector2 mousePos;
    float mousePosX;
    public Transform buttonTrans;

    List<GameObject> dataFieldList;


    // Start is called before the first frame update
    void Start() {
        //dataFieldPrefab = Resources.Load("DataField_Group") as GameObject;
        dataFieldPrefab = Resources.Load("DataField_Scroll") as GameObject;
        //contentTrans = this.transform.parent.transform;
        contentTrans = transform;

        currentIndex = ValueManager.instance.DataFieldVal["currentIndex"];
        currentNameIndex = ValueManager.instance.DataFieldVal["currentNameIndex"];
        fieldHeight = ValueManager.instance.DataFieldVal["fieldHeight"];
        contentHeight = ValueManager.instance.DataFieldVal["contentHeight"];
        contentHeightMin = ValueManager.instance.DataFieldVal["contentHeightMin"];
        plusButtonHeight = ValueManager.instance.DataFieldVal["plusButtonHeight"];
        spacingTop = ValueManager.instance.DataFieldVal["spacingTop"];
        spacingBottom = ValueManager.instance.DataFieldVal["spacingBottom"];
        spacingBetween = ValueManager.instance.DataFieldVal["spacingBetween"];
        separate = '_';
        dataFieldName = "dataField";

        dataFieldList = new List<GameObject>();

        //一開始按一次
        PlusNewDataField();
    }

    public void PlusNewDataField() {
        //生成新dataField到新的位置
        GenerateDataField();
        //移動按鈕位置
        MovePlusButton();
        //調整目前content長度
        AdjustContentHeight();
    }

    /// <summary>
    /// 生成新欄位
    /// </summary>
    private void GenerateDataField() {
        GameObject dataField;
        float newFieldPosY = spacingTop + currentIndex * (fieldHeight + spacingBetween);
        dataField = Instantiate(dataFieldPrefab, new Vector2(0.0f, 0.0f), Quaternion.identity);
        dataField.transform.SetParent(contentTrans);
        dataField.GetComponent<RectTransform>().anchoredPosition = new Vector2(0.0f, -newFieldPosY);
        dataField.name = dataFieldName + separate + currentNameIndex.ToString("000");

        dataFieldList.Add(dataField);
        //更新資料
        ValueManager.instance.DataFieldVal["currentIndex"] = currentIndex++;
        ValueManager.instance.DataFieldVal["currentNameIndex"] = currentNameIndex++;
    }

    /// <summary>
    /// 刪除欄位
    /// </summary>
    public void MinusDataField(GameObject fieldObject) {
        string fieldName = fieldObject.name.ToString();
        //更新資料
        ValueManager.instance.DataFieldVal["currentIndex"] = currentIndex--;
        //移動DataField位置
        MoveDataFieldPos(fieldName);
        //移動按鈕位置
        MovePlusButton();
        //調整目前content長度
        AdjustContentHeight();
        //刪除Field
        RemoveField(fieldObject);
    }

    /// <summary>
    /// 移動按鈕位置
    /// </summary>
    private void MovePlusButton() {
        float newPlusButtonPosY = spacingTop + currentIndex * (fieldHeight + spacingBetween);
        buttonTrans.GetComponent<RectTransform>().anchoredPosition = new Vector2(-250.0f, -newPlusButtonPosY);
    }

    /// <summary>
    /// 移動Field位置
    /// </summary>
    /// <param name="minusFieldName"></param>
    private void MoveDataFieldPos(string minusFieldName) {
        //用名稱編號去判斷之間的位置優先
        string[] minusfieldNameSplit = minusFieldName.Split('_');
        int minusFieldNum = int.Parse(minusfieldNameSplit[1]);
        foreach (GameObject dataField in dataFieldList) {
            string[] fieldNameSplit = dataField.name.Split('_');
            int fieldNum = int.Parse(fieldNameSplit[1]);
            //找到名稱編號大於要刪除的欄位(就是在他後面的)
            if (fieldNum > minusFieldNum) {
                float newFieldPosY = dataField.GetComponent<RectTransform>().anchoredPosition.y
                    + fieldHeight + spacingBetween;
                dataField.GetComponent<RectTransform>().anchoredPosition = new Vector2(0.0f, newFieldPosY);
            }
        }
        //Debug.Log(minusFieldNum);
    }

    /// <summary>
    /// 移除DataField
    /// </summary>
    private void RemoveField(GameObject removefieldObject) {
        dataFieldList.Remove(removefieldObject);
        Destroy(removefieldObject);
    }

    /// <summary>
    /// 調整content長度、給Scroll拖曳的範圍
    /// </summary>
    private void AdjustContentHeight() {
        float newContentViewHeight = spacingTop + currentIndex * (fieldHeight + spacingBetween)
            + plusButtonHeight + spacingBottom;
        if (newContentViewHeight < contentHeightMin) {
            newContentViewHeight = contentHeightMin;
        }
        float currentWidth = contentTrans.GetComponent<RectTransform>().rect.width;
        contentTrans.GetComponent<RectTransform>().sizeDelta = new Vector2(currentWidth, newContentViewHeight);
    }
}
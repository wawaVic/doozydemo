﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueManager {

    private static ValueManager _instance;
    public static ValueManager instance {
        get {
            if (_instance == null) {
                _instance = new ValueManager();
            }
            return _instance;
        }
    }

    /// <summary>
    /// ScrollDemo DataField數值 (DataField_Group)
    /// </summary>
    private Dictionary<string, float> _DataFieldVal = new Dictionary<string, float>() {
    { "fieldWidth", 550.0f }
    ,{ "fieldHeight", 140.0f }
    ,{ "spacingTop", 20.0f }
    ,{ "spacingBottom", 40.0f }
    ,{ "spacingBetween", 10.0f }
    ,{ "currentIndex", 0.0f }         //生成的Field，目前的最後一位
    ,{ "currentNameIndex", 0.0f }     //生成的Field，目前的最後一位名字
    //,{ "anchorIndex", 0.0f }        //看不到的錨點，目前生成位置
    ,{ "contentHeightMin", 584.0f }
    ,{ "contentWidth", 584.0f }
    ,{ "contentHeight", 584.0f }
    ,{ "plusButtonWidth", 50.0f }
    ,{ "plusButtonHeight", 50.0f }
    };
    public Dictionary<string, float> DataFieldVal { get { return _DataFieldVal; } set { } }

    private Dictionary<string, float> _CalendarFieldVal = new Dictionary<string, float>() {
    { "monthHeadHeight", 100.0f }
    ,{ "monthHeadWidth", 100.0f }
    ,{ "dateBodyHeight", 100.0f } //一週(一行)
    ,{ "dateTextHeight", 100.0f }
    ,{ "dateTextWidth", 100.0f }
    ,{ "sideKeep", 10.0f }
    };
    public Dictionary<string, float> CalendarFieldVal { get { return _CalendarFieldVal; } set { } }

}

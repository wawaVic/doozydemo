﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DataFieldSlideController : MonoBehaviour {

    public RectTransform content;
    bool showDeleteButton;
    float DeletePosX;
    float showDeletePosX;
    float deleteButtonHalfWidth;


    // Start is called before the first frame update
    void Start() {
        showDeletePosX = -140;
        deleteButtonHalfWidth = showDeletePosX / 2;
        
    }

    // Update is called once per frame
    void Update() {
        //左鍵放開時，判斷要不要顯示刪除按鈕
        if (Input.GetMouseButtonUp(0)) {
            if (showDeleteButton) {
                DeletePosX = showDeletePosX;
            } else {
                DeletePosX = 0.0f;
            }
            //移動field
            RectTransform rec = content.GetComponent<RectTransform>();
            rec.DOAnchorPosX(DeletePosX, 0.3f);
        }
    }
    
    /// <summary>
    /// Field有被拖曳時觸發
    /// </summary>
    public void Sliding() {
        float movingPosX = content.anchoredPosition.x;
        if (movingPosX < deleteButtonHalfWidth) {
            showDeleteButton = true;
        } else if (movingPosX > deleteButtonHalfWidth) {
            showDeleteButton = false;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HolidayDate {
    public string 西元日期;
    public string 星期;
    public string 是否放假;
    public string 備註;

    //public DateTime date;
    //public string week;
    //public int holiday;
    //public string remark;

    public DateTime GetDate() {
        DateTime dateTimeResult;
        dateTimeResult = DateTime.ParseExact(西元日期, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
        return dateTimeResult;
    }
    public string GetDateOri() {
        return 西元日期;
    }
    public int GetWeek() {
        int result = 0;
        switch (星期) {
            case "一":
                result = 1;
                break;
            case "二":
                result = 2;
                break;
            case "三":
                result = 3;
                break;
            case "四":
                result = 4;
                break;
            case "五":
                result = 5;
                break;
            case "六":
                result = 6;
                break;
            case "日":
                result = 7;
                break;
            default:
                break;
        }
        return result;
    }
    public string GetHoliday() {
        return 是否放假;
    }
    public string GetRemark() {
        return 備註;
    }
}
